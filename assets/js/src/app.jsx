import React from "react";
import ReactDOM from "react-dom";
import _ from "lodash";

import Sidebar from "./components/sidebar";
import Page from "./components/page";
import { fetchJSON, saveProject, deleteProject, saveEvent } from "./utils";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            confs: [],
            notification: "",
            isLoading: true,
            selectedProject: null,
        };
        this.pullConfs = this.pullConfs.bind(this);
        this.editProject = this.editProject.bind(this);
        this.showNewProjectForm = this.showNewProjectForm.bind(this);
        this.saveProject = this.saveProject.bind(this);
        this.deleteProject = this.deleteProject.bind(this);
        this.saveEvent = this.saveEvent.bind(this);
        this.removeNotification = this.removeNotification.bind(this);
    }

    componentDidMount() {
        this.pullConfs();
    }

    render() {
        return (
            <div>
                <nav className="navbar" role="navigation" aria-label="main navigation">
                    <div className="container">
                        <div className="navbar-brand">
                            <a className="navbar-item" href="/">
                                <img
                                    src="/images/phoenix.png"
                                    alt="Phoenix"
                                />
                            </a>
                        </div>
                    </div>
                </nav>
                <div className="container">
                    {this.showNotifications()}
                    <div className="columns">
                        <div className="column is-one-quarter">
                            <Sidebar
                                onCreateNewProject={this.showNewProjectForm}
                                onEditProject={this.editProject}
                                onSaveEvent={this.saveEvent}
                                {...this.state}
                            />
                        </div>
                        <div className="column">
                            <Page
                                isLoading={this.state.isLoading}
                                selectedProject={this.state.selectedProject}
                                onSaveProject={this.saveProject}
                                onDeleteProject={this.deleteProject}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    showNotifications() {
        if (!_.isEmpty(this.state.notification)) {
            setTimeout(this.removeNotification, 4000);
            return (
                <article className="message is-success is-small">
                    <div className="message-body">{this.state.notification}</div>
                </article>
            );
        }
    }

    removeNotification() {
        this.setState({notification: ""});
    }

    pullConfs() {
        this.setState({isLoading: true});
        return fetchJSON("/api/v1/confs").then(res => (
            this.setState({confs: res.data, isLoading: false})
        ));
    }

    editProject(projectId) {
        this.setState({selectedProject: this.getProject(projectId)});
    }

    showNewProjectForm() {
        this.setState({selectedProject: null});
    }

    saveProject(id, project) {
        saveProject(id, project).then(res => {
            this.pullConfs().then(r => {
                const selectedProject = this.getProject(res.data.project_id);
                this.setState({selectedProject: selectedProject});
            });
        });
    }

    deleteProject(id) {
        deleteProject(id).then(res => {
            this.pullConfs();
            this.setState({selectedProject: null});
        });
    }

    getProject(projectId) {
        let selectedProject = null;
        _.forEach(this.state.confs, conf => {
            if (conf.project_id == projectId) {
                selectedProject = conf;
                return;
            }
        });
        return selectedProject;
    }

    saveEvent(event) {
        saveEvent(event).then(() => (
            this.setState({notification: "Event saved!"})
        ));
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('main')
);

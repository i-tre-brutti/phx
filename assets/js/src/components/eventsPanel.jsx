import React from "react";
import _ from "lodash";

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.getEmptyState();
        this.createEvent = this.createEvent.bind(this);
    }

    render() {
        return (
            <nav className="panel">
                <p className="panel-heading">Create a new live event</p>
                {this.renderSidebar()}
            </nav>
        );
    }

    renderSidebar() {
        if (this.props.isLoading) {
            return <div className="loader" />
        }
        return this.eventsForm();
    }

    eventsForm() {
        return (
            <div className="panel-block">
                <form>
                    <div className="field">
                        <label className="label">Project</label>
                        <div className="control">
                            <div className="select">
                                <select
                                    value={this.state.project_id}
                                    onChange={this.onInputChange.bind(this, "project_id")}
                                >
                                    <option>Select the project</option>
                                    {this.getProjectsOptions()}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Event type</label>
                        <div className="control">
                            <div className="select">
                                <select
                                    value={this.state.event_type}
                                    onChange={this.onInputChange.bind(this, "event_type")}
                                >
                                    <option>Select the event type</option>
                                    <option value="new_star">Project starred</option>
                                    <option value="new_commit">New commit</option>
                                    <option value="new_pr">Pull-request opened</option>
                                    <option value="build_success">Build success</option>
                                    <option value="build_failure">Build failure</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Any extra info?</label>
                        <div className="control">
                            <textarea
                                value={this.state.extra_info}
                                onChange={this.onInputChange.bind(this, "extra_info")}
                                className="textarea"
                                placeholder="Extra info"
                            />
                        </div>
                    </div>
                    <div className="field">
                        <div className="control">
                            <button
                                className="button is-danger is-light is-fullwidth"
                                onClick={this.createEvent}
                            >
                                <span className="icon"><i className="fa fa-plus" /></span>
                                <span>Create event</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }

    getProjectsOptions() {
        return _.map(this.props.confs, conf =>
            <option key={conf.project_id} value={conf.project_id}>
                {conf.name}
            </option>
        );
    }

    onInputChange(fieldName, e) {
        const newState = _.cloneDeep(this.state);
        newState[fieldName] = e.target.value;
        this.setState(newState);
    }

    createEvent(e) {
        e.preventDefault();
        this.props.onSaveEvent(this.state);
        this.setState(this.getEmptyState());
    }

    getEmptyState() {
        return {
            project_id: "",
            event_type: "",
            extra_info: "",
        };
    }
}

import React from "react";
import _ from "lodash";
import PropTypes from 'prop-types';

import Form from "./form";

class Page extends React.Component {
    constructor(props) {
        super(props);
        this.state = {showDeleteConfirmation: false};
        this.deleteProjectBtnPressed = this.deleteProjectBtnPressed.bind(this);
        this.deleteProject = this.deleteProject.bind(this);
    }

    render() {
        if (this.props.isLoading) {
            return <div className="loader" />
        }
        return (
            <div className="content">
                {this.props.selectedProject ? this.showSelectedProject() : this.showNewProjectForm()}
            </div>
        );
    }

    showSelectedProject() {
        return (
            <div>
                <div className="level">
                    <h1 className="title level-left">{this.props.selectedProject.name}</h1>
                    <a
                        className="button is-danger delete-project level-right"
                        onClick={this.deleteProjectBtnPressed}
                    >
                        <span>Delete</span>
                        <span className="icon is-small">
                            <i className="fa fa-times" />
                        </span>
                    </a>
                </div>
                <Form project={this.props.selectedProject} onSaveProject={this.props.onSaveProject} />
                {this.showDeleteModal()}
            </div>
        );
    }

    showDeleteModal() {
        if (this.state.showDeleteConfirmation) {
            return (
                <div className="modal is-active">
                    <div className="modal-background" />
                    <div className="modal-card">
                        <header className="modal-card-head">
                            <p className="modal-card-title">Delete project "{this.props.selectedProject.name}"</p>
                            <button
                            className="delete"
                            aria-label="close"
                            onClick={() => this.setState({showDeleteConfirmation: false})}
                            />
                        </header>
                        <section className="modal-card-body">
                            <p>Do you want to delete the project?</p>
                            <div className="level">
                                <div className="level-item has-text-centered">
                                    <button
                                        className="button is-danger"
                                        onClick={this.deleteProject}
                                    >Yes, nuke it!</button>
                                </div>
                                <div className="level-item has-text-centered">
                                    <button
                                        className="button"
                                        onClick={() => this.setState({showDeleteConfirmation: false})}
                                    >Wooops, nope, wrong button!</button>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            );
        }
    }

    showNewProjectForm() {
        return (
            <div>
                <h1 className="title">New Project</h1>
                <Form onSaveProject={this.props.onSaveProject} />
            </div>
        );
    }

    deleteProjectBtnPressed() {
        this.setState({showDeleteConfirmation: true});
    }

    deleteProject() {
        this.setState({showDeleteConfirmation: false});
        this.props.onDeleteProject(this.props.selectedProject.id)
    }
}

Page.propTypes = {
    isLoading: PropTypes.bool,
    selectedProject: PropTypes.object,
    onSaveProject: PropTypes.func,
    onDeleteProject: PropTypes.func
};

export default Page;

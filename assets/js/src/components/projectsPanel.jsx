import React from "react";
import _ from "lodash";

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.createNewProject = this.createNewProject.bind(this);
        this.editProject = this.editProject.bind(this);
    }

    render() {
        return (
            <nav className="panel">
                <p className="panel-heading">Available projects</p>
                {this.renderSidebar()}
                <div className="panel-block">
                    <button
                        className="button is-primary is-light is-fullwidth"
                        onClick={this.createNewProject}
                    >
                        <span className="icon"><i className="fa fa-plus" /></span>
                        <span>New project</span>
                    </button>
                </div>
            </nav>
        );
    }

    renderSidebar() {
        if (this.props.isLoading) {
            return <div className="loader" />
        }
        return this.confsList();
    }

    confsList() {
        return _.map(this.props.confs, conf =>
            <a
                className={`panel-block ${this.getElementClass(conf.project_id)}`}
                key={conf.project_id}
                onClick={this.editProject} data-id={conf.project_id}
            >
                {conf.name}
            </a>
        );
    }

    createNewProject(e) {
        e.preventDefault();
        this.props.onCreateNewProject();
    }

    editProject(e) {
        e.preventDefault();
        this.props.onEditProject(e.target.dataset.id);
    }

    getElementClass(project_id) {
        if (!_.isNil(this.props.selectedProject) && project_id == this.props.selectedProject.project_id) {
            return "is-active";
        }
    }
}

import React from "react";

import ProjectsPanel from "./projectsPanel";
import EventsPanel from "./eventsPanel";

export default class extends React.Component {
    render() {
        return (
            <div>
                <ProjectsPanel {...this.props} />
                <EventsPanel {...this.props} />
            </div>
        );
    }
}

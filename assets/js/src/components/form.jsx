import React from "react";
import PropTypes from 'prop-types';
import _ from "lodash";

class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {tmpProject: this.getBaseProject()};
        this.addSource = this.addSource.bind(this);
        this.addIssue = this.addIssue.bind(this);
        this.saveForm = this.saveForm.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const project = _.cloneDeep(nextProps.project);
        if (_.has(project, "id")) {
            delete(project.id);
        }
        const tmpProject = _.isNil(nextProps.project) ? this.getBaseProject() : project;
        this.setState({tmpProject: tmpProject});
    }

    render() {
        let {tmpProject} = this.state;

        return (
            <form>
                <div className="field">
                    <label className="label">ID</label>
                    <div className="control">
                        <input
                            className="input"
                            type="text"
                            placeholder="Project ID (unique, required)"
                            onChange={this.onChange.bind(this, "project_id")}
                            value={tmpProject.project_id}
                        />
                    </div>
                </div>

                <div className="field">
                    <label className="label">Name</label>
                    <div className="control">
                        <input
                            className="input"
                            type="text"
                            placeholder="Name"
                            onChange={this.onChange.bind(this, "name")}
                            value={tmpProject.name}
                        />
                    </div>
                </div>

                <h3>Sources</h3>
                {this.sourcesForm()}

                <h3>Issues</h3>
                {this.issuesForm()}

                <div className="field">
                    <div className="control">
                        <button
                            className="button is-primary"
                            onClick={this.saveForm}
                            disabled={!this.validateForm()}
                        >Save</button>
                    </div>
                </div>
            </form>
        );
    }

    onChange(fieldName, e) {
        const tmpProject = this.state.tmpProject;
        tmpProject[fieldName] = e.target.value;
        this.setState({tmpProject: tmpProject});
    }

    repoTypeSelection(confType, repoType, projectID) {
        const repoTypes = ["gitlab", "github"];
        let options = _.map(repoTypes, r => (
            <option key={r} value={r}>{r}</option>
        ));
        options = [<option key="" value="">Select a repo type</option>].concat(options)
        return (
            <div className="select">
                <select value={repoType} onChange={this.updateRepoType.bind(this, confType, repoType, projectID)}>
                    {options}
                </select>
            </div>
        );
    }

    updateRepoType(confType, repoType, projectID, e) {
        const list = [];
        _.forEach(this.state.tmpProject[confType], l => {
            if (l.repoType === repoType && l.conf.projectID === projectID) {
                l.repoType = e.target.value;
            }
            list.push(l);
        });
        const tmpProject = this.state.tmpProject;
        tmpProject[confType] = list;
        this.setState({tmpProject: tmpProject});
    }

    sourcesForm() {
        let index = 0;
        const fields = _.map(this.state.tmpProject.sources, src => (
            <div className="message is-link" key={`src-block-${index++}`}>
                <div className="message-header">
                    <a className="delete" onClick={this.removeSource.bind(this, src.repoType, src.conf.projectID)} />
                    {this.repoTypeSelection("sources", src.repoType, src.conf.projectID)}
                </div>
                <div className="message-body">
                    <div className="field">
                        <label className="label">Project ID</label>
                        <div className="control">
                            <input
                                className="input"
                                type="text"
                                placeholder="Project ID"
                                onChange={this.updateSource.bind(this, src.repoType, src.conf.projectID, "projectID")}
                                value={src.conf.projectID}
                            />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Auth Token</label>
                        <div className="control">
                            <input
                                className="input"
                                type="text"
                                placeholder="Auth Token"
                                onChange={this.updateSource.bind(this, src.repoType, src.conf.authToken, "authToken")}
                                value={src.conf.authToken}
                            />
                        </div>
                    </div>
                </div>
            </div>
        ));

        return (
            <div className="content">
                {fields}
                <div className="field">
                    <div className="control">
                        <a className="button is-small is-link" onClick={this.addSource}>New source</a>
                    </div>
                </div>
            </div>

        );
    }

    issuesForm() {
        let index = 0;
        const fields = _.map(this.state.tmpProject.issues, issue => (
            <div className="message is-info" key={`issue-block-${index++}`}>
                <div className="message-header">
                    <a className="delete" onClick={this.removeIssue.bind(this, issue.repoType, issue.conf.projectID)} />
                    {this.repoTypeSelection("issues", issue.repoType, issue.conf.projectID)}
                </div>
                <div className="message-body">
                    <div className="field">
                        <label className="label">Project ID</label>
                        <div className="control">
                            <input
                                className="input"
                                type="text"
                                placeholder="Project ID"
                                onChange={this.updateIssue.bind(this, issue.repoType, issue.conf.projectID, "projectID")}
                                value={issue.conf.projectID}
                            />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Auth Token</label>
                        <div className="control">
                            <input
                                className="input"
                                type="text"
                                placeholder="Auth Token"
                                onChange={this.updateIssue.bind(this, issue.repoType, issue.conf.authToken, "authToken")}
                                value={issue.conf.authToken}
                            />
                        </div>
                    </div>
                </div>
            </div>
        ));

        return (
            <div className="content">
                {fields}
                <div className="field">
                    <div className="control">
                        <a className="button is-small is-info" onClick={this.addIssue}>New issue</a>
                    </div>
                </div>
            </div>

        );
    }

    addSource() {
        if (!this.checkCanAdd(this.state.tmpProject.sources)) {
            alert("Cannot add multiple empty sources, please fill in the existing one");
            return;
        }
        const tmpProject = _.cloneDeep(this.state.tmpProject);
        const sources = _.cloneDeep(tmpProject.sources);
        sources.push(this.getBaseSourceOrIssue());
        tmpProject.sources = sources;
        this.setState({tmpProject: tmpProject});
    }

    addIssue() {
        if (!this.checkCanAdd(this.state.tmpProject.issues)) {
            alert("Cannot add multiple empty issues, please fill in the existing one");
            return;
        }
        const tmpProject = _.cloneDeep(this.state.tmpProject);
        const issues = _.cloneDeep(tmpProject.issues);
        issues.push(this.getBaseSourceOrIssue());
        tmpProject.issues = issues;
        this.setState({tmpProject: tmpProject});
    }

    checkCanAdd(list) {
        let canAdd = true;
        _.forEach(list, l => {
            if (l.repoType === "" && l.conf.projectID === "") {
                canAdd = false;
            }
        });

        return canAdd;
    }

    updateSource(repoType, initialValue, field, e) {
        const sources = []
        _.forEach(this.state.tmpProject.sources, s => {
            if (s.repoType === repoType && s.conf[field] === initialValue) {
                s.conf[field] = e.target.value;
            }
            sources.push(s);
        });
        const tmpProject = this.state.tmpProject;
        tmpProject.sources = sources;
        this.setState({tmpProject: tmpProject});
    }

    updateIssue(repoType, initialValue, field, e) {
        const issues = []
        _.forEach(this.state.tmpProject.issues, i => {
            if (i.repoType === repoType && i.conf[field] === initialValue) {
                i.conf[field] = e.target.value;
            }
            issues.push(i);
        });
        const tmpProject = this.state.tmpProject;
        tmpProject.issues = issues;
        this.setState({tmpProject: tmpProject});
    }

    removeSource(repoType, projectID) {
        const newSources = _.filter(this.state.tmpProject.sources, (s) => (
            s.repoType !== repoType || s.conf.projectID !== projectID
        ));
        const tmpProject = _.cloneDeep(this.state.tmpProject);
        tmpProject.sources = newSources;
        this.setState({ tmpProject: tmpProject });
    }

    removeIssue(repoType, projectID) {
        const newIssues = _.filter(this.state.tmpProject.issues, (i) => (
            i.repoType !== repoType || i.conf.projectID !== projectID
        ));
        const tmpProject = _.cloneDeep(this.state.tmpProject);
        tmpProject.issues = newIssues;
        this.setState({ tmpProject: tmpProject });
    }

    getBaseProject() {
        return {
            project_id: "",
            name: "",
            sources: [],
            issues: []
        }
    }

    getBaseSourceOrIssue() {
        return {
            repoType: "",
            conf: {
                authToken: "",
                projectID: ""
            }
        };
    }

    saveForm(e) {
        e.preventDefault();
        if (!this.validateForm()) {
            alert("Please fill in all the fields.")
            return
        }
        const projectId = !_.isNil( this.props.project) ? this.props.project.id : null;
        this.props.onSaveProject(projectId, this.state.tmpProject);
    }

    validateForm() {
        const p = this.state.tmpProject;
        // basic fields mustn't be empty
        if (_.isEmpty(p.project_id) || _.isEmpty(p.name)) {
            return false;
        }
        // all sources must be filled in
        let res = true;
        _.forEach(p.sources, s => {
            if (_.isEmpty(s.repoType) || _.isEmpty(s.conf.projectID)) {
                res = false;
            }
        });
        if (!res) {
            return false;
        }

        // all issues must be filled in
        _.forEach(p.issues, i => {
            if (_.isEmpty(i.repoType) || _.isEmpty(i.conf.projectID)) {
                res = false;
            }
        });
        if (!res) {
            return false;
        }

        return true;
    }
}

Form.propTypes = {
    project: PropTypes.shape({
        project_id: PropTypes.string,
        name: PropTypes.string,
    }),
    onSaveProject: PropTypes.func
};

export default Form;

import _ from "lodash";

export const fetchJSON = (url) => {
    return fetch(url).then(resp => {
        return resp.json().then(res => {
            return res;
        });
    });
}

export const saveProject = (id, project) => {
    let url = "/api/v1/confs";
    let method = "POST";
    if (!_.isNil(id) && !_.isEmpty(id)) {
        url = `${url}/${id}`
        method = "PATCH";
    }
    return fetch(url, {
        method: method,
        body: JSON.stringify({project: project}),
        headers: new Headers({
            'Content-Type': 'application/json'
        })
    }).then(resp => {
        return resp.json().then(res => {
            return res;
        });
    });
}

export const deleteProject = (id) => {
    let url = `/api/v1/confs/${id}`;
    return fetch(url, {
        method: "DELETE",
    });
}

export const saveEvent = (event) => {
    return fetch("/api/v1/live_events", {
        method: "POST",
        body: JSON.stringify({event: event}),
        headers: new Headers({
            'Content-Type': 'application/json'
        })
    }).then(resp => {
        if (!resp.ok) {
            throw new Error();
        }
        return resp;
    });
}

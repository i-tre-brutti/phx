defmodule PhxWeb.Router do
  use PhxWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", PhxWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api", PhxWeb.Api do
    pipe_through :api
    scope "/v1", V1 do
      resources "/confs", ConfController, except: [:new, :edit]
      resources "/live_events", EventsController, only: [:index, :create]
    end
  end
end

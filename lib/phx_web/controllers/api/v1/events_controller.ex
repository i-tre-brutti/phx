defmodule PhxWeb.Api.V1.EventsController do
  use PhxWeb, :controller

  def index(conn, %{"project_id" => project_id}) do
    cursor = Mongo.find(:mongo, "live_events", %{"project_id" => project_id})
    events = Enum.to_list(cursor)
    render conn, "index.json", events: events
  end

  def create(conn, %{"event" => event_params}) do
    event_params = Map.put(event_params, "timestamp", DateTime.utc_now)
    case Mongo.insert_one(:mongo, "live_events",
    event_params,
      []
    ) do
      {:ok, result} ->
        conn
        |> send_resp(200, "created")
      {:error, changeset} ->
        conn
        |> send_resp(400, changeset)
    end
  end
end

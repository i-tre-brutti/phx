defmodule PhxWeb.Api.V1.ConfController do
  use PhxWeb, :controller

  def index(conn, _params) do
    cursor = Mongo.find(:mongo, "confs", %{})
    confs = Enum.to_list(cursor)
    render conn, "index.json", confs: confs
  end

  def show(conn, %{"id" => project_id}) do
    conf = Mongo.find_one(:mongo, "confs", %{"project_id" => project_id})
    if conf == nil do
      conn
      |> send_resp(404, "")
    else
      render conn, "show.json", conf: conf
    end
  end

  def create(conn, %{"project" => project_params}) do
    case Mongo.insert_one(:mongo, "confs",
      project_params,
      []
    ) do
      {:ok, result} ->
        %{:inserted_id => object_id} = result
        conf = Mongo.find_one(:mongo, "confs", %{"_id" => object_id})
        render conn, "show.json", conf: conf
      {:error, changeset} ->
        conn
        |> send_resp(400, changeset)
    end
  end

  def update(conn, %{"id" => id, "project" => project_params}) do
    object_id = BSON.ObjectId.decode!(id)
    case Mongo.find_one_and_update(:mongo, "confs",
      %{"_id" => object_id},
      %{"$set" => project_params },
      [return_document: :after]
    ) do
      {:ok, conf} ->
        render conn, "show.json", conf: conf
      {:error, changeset} ->
        conn
        |> send_resp(400, changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    object_id = BSON.ObjectId.decode!(id)
    case Mongo.find_one_and_delete(:mongo, "confs",
      %{"_id" => object_id},
      [return_document: :after]
    ) do
      {:ok, _} ->
        conn
        |> send_resp(204, "")
      {:error, changeset} ->
        conn
        |> send_resp(400, changeset)
    end
  end
end

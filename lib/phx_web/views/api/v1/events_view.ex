defmodule PhxWeb.Api.V1.EventsView do
  use PhxWeb, :view

  def render("index.json", %{events: events}) do
    %{data: render_many(events, __MODULE__, "event.json")}
  end

  def render("event.json", %{events: event}) do
    %{
      project_id: event["project_id"],
      timestamp: event["timestamp"],
      event_type: event["event_type"],
      extra_info: event["extra_info"]
    }
  end
end

defmodule PhxWeb.Api.V1.ConfView do
  use PhxWeb, :view

  def render("index.json", %{confs: confs}) do
    %{data: render_many(confs, __MODULE__, "conf.json")}
  end

  def render("show.json", %{conf: conf}) do
    %{data: render_one(conf, __MODULE__, "conf.json")}
  end

  def render("conf.json", %{conf: conf}) do
    %{
      id: BSON.ObjectId.encode!(conf["_id"]),
      project_id: conf["project_id"],
      name: conf["name"],
      sources: conf["sources"],
      issues: conf["issues"]
    }
  end
end

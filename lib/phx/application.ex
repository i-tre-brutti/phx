defmodule Phx.Application do
  use Application

  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec

    # Define workers and child supervisors to be supervised
    children = [
      # Start the Ecto repository
      # supervisor(Phx.Repo, []),
      # Start the endpoint when the application starts
      supervisor(PhxWeb.Endpoint, []),
      # Start your own worker by calling: Phx.Worker.start_link(arg1, arg2, arg3)
      # worker(Phx.Worker, [arg1, arg2, arg3]),
      worker(Mongo, [[database:
        Application.get_env(:phx, :db)[:name], name: :mongo]])
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Phx.Supervisor]

    result = Supervisor.start_link(children, opts)
    Phx.Startup.ensure_indexes

    result
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    PhxWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end

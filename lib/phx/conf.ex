defmodule Phx.Conf do
  # use Phx.Web, :model
  # use Phx, :model
  use Ecto.Schema
  import Ecto.Changeset

  require Logger

  schema "confs" do
    field :project_id, :string
    field :name,       :string
  end

  def changeset_new_user(conf, params \\ %{}) do
    conf
      |> cast(params, [:project_id, :name])
      |> validate_required([:project_id])
  end
end

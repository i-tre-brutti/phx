# Phx

Phx is a web app to help us configure projects and it's written in Elixir using the Phoenix
framework and React.

To work with Phx you need a running MongoDB server and a proper installation of the Elixir language.

To locally start the app:

  * Install dependencies with `mix deps.get`
  * Configure the db connection in `config/dev.exs`
  * Install Node.js dependencies with `cd assets && yarn install && cd ..` (use `npm` if you don't have `yarn`)
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

### Installation instructions for Ubuntu

```
wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb && sudo dpkg -i erlang-solutions_1.0_all.deb
sudo apt-get update
sudo apt-get install esl-erlang elixir nodejs npm inotify-tools
mix local.hex
mix archive.install https://github.com/phoenixframework/archives/raw/master/phx_new.ez
sudo npm install -g yarn
```

Clone the repo and access the folder using the terminal. Then run:

```
mix deps.get
cd assets && yarn install && cd ..
mix phx.server
```

If the server ran correctly you can open the app at http://localhost:4000/

## Contributing

The backend app is a simple Phoenix app, check the official docs.

The frontend app is written using React, the code is in the `assets/js/src/` folder.

## Learn more on Phoenix and Elixir

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
